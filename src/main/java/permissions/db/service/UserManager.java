package permissions.db.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.db.domain.User;

public class UserManager {

	private Connection connection;

	private String url = "jdbc:hsqldb:hsql://localhost/workdb";

	private String createTableUser = "CREATE TABLE User(id bigint GENERATED BY DEFAULT AS IDENTITY, username varchar(20), password varchar(20))";

	private PreparedStatement addUserStmt;
	private PreparedStatement deleteAllUserStmt;
	private PreparedStatement getAllUserStmt;

	private Statement statement;

	public UserManager() {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("User".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createTableUser);

			addUserStmt = connection
					.prepareStatement("INSERT INTO User (username, password) VALUES (?, ?)");
			deleteAllUserStmt = connection
					.prepareStatement("DELETE FROM User");
			getAllUserStmt = connection
					.prepareStatement("SELECT id, username, password FROM User");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	void clearPersons() {
		try {
			deleteAllUserStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int addUser(User user) {
		int count = 0;
		try {
			addUserStmt.setString(1, user.getUsername());
			addUserStmt.setString(2, user.getPassword());

			count = addUserStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public List<User> getAllUser() {
		List<User> user = new ArrayList<User>();

		try {
			ResultSet rs = getAllUserStmt.executeQuery();

			while (rs.next()) {
				User p = new User();
				p.setId(rs.getInt("id"));
				p.setUsername(rs.getString("username"));
				p.setPassword(rs.getString("password"));
				user.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

}
