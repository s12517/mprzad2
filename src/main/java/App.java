import permissions.db.domain.*;
import permissions.db.service.*;
public class App {
 
        public static void main(String[] args) {
                Person person = new Person();
                person.setName("Jan Kowalski");
                person.setYob(1990);
                PersonManager personManager = new PersonManager();
                personManager.addPerson(person);
                System.out.println(person);
                
                Address address = new Address();
                address.setCity("Sin");
                address.setCountry("Polska");
                address.setStreet("Jałowa");
                address.setPostalCode("30-800");
                address.setHouseNumber("3");
                address.setLocalNumber("12");
                AddressManager addressManager = new AddressManager();
                addressManager.addAddress(address);
                
                Permission permission = new Permission();
                permission.setName("Full");
                PermissionManager permissionManager = new PermissionManager();
                permissionManager.addPermission(permission);
                
                User user = new User();
                user.setUsername("SweetLikeSugar");
                user.setPassword("landrynka");
                UserManager userManager = new UserManager();
                userManager.addUser(user);
                
                Role role = new Role();
                role.setName("Ważna");
                RoleManager roleManager = new RoleManager();
                roleManager.addRole(role);
        }
 
}

